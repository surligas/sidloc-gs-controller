# SIDLOC Ground Station Controller image
#
# Copyright (C) 2024 Libre Space Foundation <https://libre.space/>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

ARG GNURADIO_IMAGE_TAG=3.10.5.1-slim
FROM librespace/gnuradio:${GNURADIO_IMAGE_TAG}
LABEL org.opencontainers.image.authors='Vasilis Tsiligiannis <acinonyx@openwrt.gr>'

ARG SIDLOC_GS_CONTROLLER_UID=500
ARG SIDLOC_GS_CONTROLLER_VARSTATEDIR=/var/lib/sidloc-gs-controller

# Add unprivileged system user
RUN groupadd -r -g ${SIDLOC_GS_CONTROLLER_UID} sidloc-gs-controller \
	&& useradd -r -u ${SIDLOC_GS_CONTROLLER_UID} \
		-g sidloc-gs-controller \
		-d ${SIDLOC_GS_CONTROLLER_VARSTATEDIR} \
		-s /bin/false \
		-G audio,dialout,plugdev \
		sidloc-gs-controller

# Create application varstate directory
RUN install -d -m 1777 -o ${SIDLOC_GS_CONTROLLER_UID} -g ${SIDLOC_GS_CONTROLLER_UID} ${SIDLOC_GS_CONTROLLER_VARSTATEDIR}

# Copy packages list
COPY packages.debian /usr/local/src/sidloc-gs-controller/packages.debian

# Install system packages
RUN apt-get update \
	&& xargs -a /usr/local/src/sidloc-gs-controller/packages.debian apt-get install -qy git pipx \
	&& rm -r /var/lib/apt/lists/*

# Download USRP images
RUN uhd_images_downloader -q

# Copy flowgraphs
COPY flowgraphs /usr/local/src/sidloc-gs-controller/flowgraphs

# Compile GNU Radio flowgraph
RUN TMP_GRCC_OUTPUT_DIR="$(mktemp -d)" \
	&& grcc -o "$TMP_GRCC_OUTPUT_DIR" /usr/local/src/sidloc-gs-controller/flowgraphs/*.grc \
	&& install -m 0755 "$TMP_GRCC_OUTPUT_DIR"/*.py /usr/local/bin \
	&& rm -rf "$TMP_GRCC_OUTPUT_DIR"

# Copy source code
COPY . /usr/local/src/sidloc-gs-controller/

# Install Python dependencies and application
RUN echo "[global]" > /etc/pip.conf \
	&& echo "extra-index-url=https://www.piwheels.org/simple" >> /etc/pip.conf \
	&& PIPX_HOME=/opt/pipx \
		PIPX_BIN_DIR=/usr/local/bin \
		PIPX_MAN_DIR=/usr/local/share/man \
		pipx install \
		--system-site-packages \
		--pip-args="--no-cache-dir -c /usr/local/src/sidloc-gs-controller/constraints.txt" \
		/usr/local/src/sidloc-gs-controller \
	&& PIPX_HOME=/opt/pipx \
		PIPX_BIN_DIR=/usr/local/bin \
		PIPX_MAN_DIR=/usr/local/share/man \
		pipx runpip sidloc-gs-controller install \
		--no-cache-dir \
		--no-deps \
		-r /usr/local/src/sidloc-gs-controller/requirements.txt
