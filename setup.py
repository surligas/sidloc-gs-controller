import versioneer
from setuptools import setup

setup_args = {
    'version': versioneer.get_version(),
    'cmdclass': versioneer.get_cmdclass(),
}

setup(**setup_args)
