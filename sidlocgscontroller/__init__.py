"""SIDLOC Ground Station Controller module initialization"""
import sys

from ._version import get_versions

__version__ = get_versions()['version']

del get_versions


def main():
    """SIDLOC Ground Station Controller"""

    sys.exit(0)
